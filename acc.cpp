#include "acc.h"
#include "stdlib.h"
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include <thread>

ACCData::ACCData() {
    TCHAR szName1[] = TEXT("Local\\acpmf_physics");
    m_physics.map_file = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(SPageFilePhysics), szName1);
    m_physics.map_file_buff = (unsigned char*)MapViewOfFile(m_physics.map_file, FILE_MAP_READ, 0, 0, sizeof(SPageFilePhysics));
    pf_physics = (SPageFilePhysics*)m_physics.map_file_buff;

    TCHAR szName2[] = TEXT("Local\\acpmf_graphics");
    m_graphics.map_file = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(SPageFileGraphic), szName2);
    m_graphics.map_file_buff = (unsigned char*)MapViewOfFile(m_graphics.map_file, FILE_MAP_READ, 0, 0, sizeof(SPageFileGraphic));
    pf_graphics = (SPageFileGraphic*)m_graphics.map_file_buff;

    TCHAR szName3[] = TEXT("Local\\acpmf_static");
    m_static.map_file = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(SPageFileStatic), szName3);
    m_static.map_file_buff = (unsigned char*)MapViewOfFile(m_static.map_file, FILE_MAP_READ, 0, 0, sizeof(SPageFileStatic));
    pf_static = (SPageFileStatic*)m_physics.map_file_buff;
};
