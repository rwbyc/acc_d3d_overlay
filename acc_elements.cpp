#pragma once
#include <iostream>
#include <array>
#include "acc_elements.h"

// XXX: WidgetData::drawFontSprites doesn't affect origin it's always lined up by 0,0 ? using it wrong?
const DirectX::XMFLOAT2 fontOrigin = Vector2(0.0f, 0.0f);

void updateWidgetValueByLayout(Widget* widget, float value)
{
    if (widget->getDrawLayout() == WidgetDrawLayout::HEIGHT)
    {
        widget->set_value(value * widget->height);

    } else if (widget->getDrawLayout() == WidgetDrawLayout::WIDTH)
    {
        widget->set_value(value * widget->width);
    }
}

void drawDropinOverlay(Widget* thisWidget, DirectX::PrimitiveBatch<VPC>* p_prim_batch, WidgetData* wData, float mouseX, float mouseY)
{
    for (Widget* widget: wData->widgetList->widgets)
    {
        if (widget->get_type() != WidgetType::CONTAINER) continue;
        WidgetContainer<Widget*>* widgetCont = dynamic_cast<WidgetContainer<Widget*>*>(widget);
        if (widgetCont && widgetCont->xy_inside(mouseX, mouseY))
        {
            thisWidget->set_last_dropin(widgetCont);
            widgetCont->draw_dropin_overlay(p_prim_batch, mouseX, mouseY);
        }
    }
};

WidgetData::WidgetData(ACCData* acc, D3D11_VIEWPORT* pViewPort, DirectX::SpriteFont* g_pSpriteFont)
{
    this->acc = acc;
    this->pViewport = pViewPort;
    this->pSpriteFont = g_pSpriteFont;

    this->pKeyTracker = new KeyTracker();
    this->mousePos = new MousePos();
    this->widgetSpawner= new WidgetSpawner<WidgetIconResource*>(this);
    this->widgetList = new WidgetList<Widget*>();
    this->configHelper = new ConfigHelper(this);

    this->userBinds = {
        {BindActions::TOGGLE_WIDGETSPAWNER, VK_F3},
        {BindActions::TOGGLE_LAYOUT, VK_END},
        {BindActions::LMBUTTON, VK_SPACE},
        {BindActions::MMBUTTON, VK_MBUTTON},
        {BindActions::RM_WIDGET, VK_DELETE},
        {BindActions::SAVE_CONFIG, VK_F5},
        {BindActions::TOGGLE_DRAW_LAYOUT, VK_F7},
        // {BindActions::LOAD_CONFIG, VK_F9},
    };
    for (auto userbind: this->userBinds)
    {
        this->pKeyTracker->track_button(userbind.second);
    }

    this->configHelper->readConfig(this->widgetList);
    this->widgetList->add_widget(this->widgetSpawner);

    // TODO: make every Widget Constructor Require a WidgetData*
    for (auto widget: this->widgetList->widgets)
    {
        std::cout << "wData ptr addr:" << this << std::endl;
        widget->wData = this;
    }
}

void WidgetData::handleUserInputs(DirectX::PrimitiveBatch<VPC>* p_prim_batch)
{
    this->mousePos->update_pos();

    for (auto userbinds: this->userBinds)
    {
        if (this->pKeyTracker->pressed(userbinds.second))
        {
            // This needs to be seperate because the user can't put the mouse inside the offscreen widget
            if (userbinds.second == this->get_bind(BindActions::TOGGLE_WIDGETSPAWNER))
            {
                this->widgetSpawner->handle_inputs(this, KeyStates::PRESSED, userbinds.second, this->mousePos->x, this->mousePos->y, p_prim_batch);
                return;
            }

            if (userbinds.second == this->get_bind(BindActions::SAVE_CONFIG))
            {
                std::cout << "Saving Config" << std::endl;
                this->configHelper->saveConfig();
                return;
            }

            // if (userbinds.second == this->get_bind(BindActions::LOAD_CONFIG))
            // {
            //     // TODO: do this automagically or empty list beforehand?
            //     std::cout << "Reading Config" << std::endl;
            //     this->configHelper->readConfig(this->widgetList);
            //     return;
            // }

            // std::cout << "widgetListSize: " << this->widgetList->widgets.size() << std::endl; 
            for (Widget* widget: this->widgetList->widgets)
            {
                // std::cout << "pressed widget: " << widget << std::endl;
                if ((!(this->widgetList->moving_widget_inside)) && widget->xy_inside(this->mousePos->x, this->mousePos->y))
                {
                    // std::cout << "pressed inside widget: " << widget << std::endl;
                    this->lastPressedWidgets.push_back(widget);
                    widget->handle_inputs(this, KeyStates::PRESSED, userbinds.second, this->mousePos->x, this->mousePos->y, p_prim_batch);
                    if (widget->is_moving)
                    {
                        this->widgetList->moving_widget_inside = true;
                    }
                }
            }
        }
        else if(this->pKeyTracker->released(userbinds.second))
        {
            bool found = false;
            while (!this->lastPressedWidgets.empty())
            {
                Widget* widget = this->lastPressedWidgets.back();
                widget->handle_inputs(this, KeyStates::RELEASED, userbinds.second, this->mousePos->x, this->mousePos->y, p_prim_batch);
                if (widget->is_moving)
                {
                    found = true;
                }
                this->lastPressedWidgets.pop_back();
            }
            this->widgetList->moving_widget_inside = found;
        }
    }
}

int WidgetData::get_bind(BindActions action)
{
    userBindsT::const_iterator it = this->userBinds.find(action);
    if ( it == this->userBinds.end())
    {
        return 0;
    } else
    {
        return it->second;
    };
}

template<class T>
void WidgetList<T>::add_widget(T widget)
{
    if (widget->get_type() == WidgetType::CONTAINER)
    {
        WidgetContainer<Widget*>* widgetCont = dynamic_cast<WidgetContainer<Widget*>*>(widget);
        if (widgetCont)
        {
            this->widgets.push_back(widgetCont);
            for (Widget* childWidget: widgetCont->childs.widgets)
            {
                this->widgets.push_back(childWidget);
            }
        }
    } else {
        this->widgets.push_back(widget);
    }
}

XMFLOAT4 Widget::get_color(ColorType type)
{
    return this->colorMap[type];
}

void GearWidget::update_value(ACCData* acc)
{
    int gear = acc->currentGear();
    if (gear == 0)
    { 
        this->value = L"R";
    }
    else if (gear == 1)
    {
        this->value = L"N";
    }
    else
    {
        this->value = std::to_wstring(gear - 1);
    }
}

void GearWidget::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    Widget::draw(p_prim_batch, mouseX, mouseY);
    this->gearSprite->color = this->get_color(ColorType::FOREGROUND);
    this->gearSprite->setOutputText(
        this->value,
        this->wData->getSpriteFont()
    );
    this->gearSprite->position = Vector2(
        this->left + this->border_width,
        this->top + this->height / 2.0f
    );
    this->wData->queueFontSprite(this->gearSprite);
}

ThrottleWidget::ThrottleWidget()
{
    this->setID(uuid::generate_uuid_v4());
}

XMFLOAT4 ThrottleWidget::get_color(ColorType type)
{
    if (this->colorMap.find(type) != this->colorMap.end())
    {
        return this->colorMap[type];
    } else {
        return Widget::get_color(type);
    }
}

void ThrottleWidget::update_value(ACCData* acc)
{
    updateWidgetValueByLayout(this, acc->getThrottle());
}

void ThrottleWidget::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    Widget::draw(p_prim_batch, mouseX, mouseY);
    this->drawValueRect(p_prim_batch);

    this->tcSprite->setOutputText(
        utils::addValueToWString(this->tcSpriteText, this->wData->getACC()->getTC()),
        this->wData->getSpriteFont()
    );
    this->tcSprite->position = Vector2(
        this->left + this->border_width,
        this->top + this->height / 2.0f
    );
    this->wData->queueFontSprite(this->tcSprite);
};

BrakeWidget::BrakeWidget()
{
    this->setID(uuid::generate_uuid_v4());
}

XMFLOAT4 GearWidget::get_color(ColorType type)
{
    if (this->colorMap.find(type) != this->colorMap.end())
    {
        return this->colorMap[type];
    } else {
        return Widget::get_color(type);
    }
}

XMFLOAT4 BrakeWidget::get_color(ColorType type)
{
    if (this->colorMap.find(type) != this->colorMap.end())
    {
        return this->colorMap[type];
    } else {
        return Widget::get_color(type);
    }
}

void BrakeWidget::update_value(ACCData* acc)
{
    updateWidgetValueByLayout(this, acc->getBrake());
}

void BrakeWidget::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    Widget::draw(p_prim_batch, mouseX, mouseY);
    this->drawValueRect(p_prim_batch);

    this->absSprite->setOutputText(
        utils::addValueToWString(this->absSpriteText, this->wData->getACC()->getABS()),
        this->wData->getSpriteFont()
    );
    this->absSprite->position = Vector2(
        this->left + this->border_width,
        this->top + this->height / 2.0f
    );
    this->wData->queueFontSprite(this->absSprite);
};

bool Widget::xy_inside(float mx, float my)
{
    if  (mx > this->left && mx < (this->left + this->width))
    {
        if (my > this->top && my < (this->top + this->height))
        {
            return true;
        }
    }
    return false;
}

void Widget::set_size(float width, float height)
{
    // TODO: Some constraint here?
    this->width = width;
    this->height = height;
}

void Widget::draw_outline(PrimitiveBatch<VPC>* p_prim_batch, int line_width = 3, bool drawAsBorder = false)
{
    ColorType ct;
    float left, top, width, height;
    if (drawAsBorder)
    {
        left = this->left + this->border_width;
        top = this->top + this->border_width;
        width = this->width - this->border_width*2;
        height = this->height - this->border_width*2;
        ct = ColorType::BORDER_COLOR;
    } else 
    {
        left = this->left;
        top = this->top;
        width = this->width;
        height = this->height;
        ct = ColorType::EDIT_MODE;
    }
    for (int i = 0; i < line_width; i++) 
    {
        Vector3 p1 = Vector3(left - (i + 1), top - i, this->z_plane);
        Vector3 p2 = Vector3(left - i, top + height + i, this->z_plane);
        Vector3 p3 = Vector3(left + width + i, top + height + i, this->z_plane);
        Vector3 p4 = Vector3(left + width + i, top -i , this->z_plane);

        p_prim_batch->DrawLine(
            VPC(Vector3(p1), this->get_color(ct)),
            VPC(Vector3(p2), this->get_color(ct))
        );
        p_prim_batch->DrawLine(
            VPC(Vector3(p2), this->get_color(ct)),
            VPC(Vector3(p3), this->get_color(ct))
        );
        p_prim_batch->DrawLine(
            VPC(Vector3(p3), this->get_color(ct)),
            VPC(Vector3(p4), this->get_color(ct))
        );
        p_prim_batch->DrawLine(
            VPC(Vector3(p4), this->get_color(ct)),
            VPC(Vector3(p1), this->get_color(ct))
        );
    }
}

void Widget::update_value(ACCData* acc)
{
    this->value = 0.f;
}

void Widget::set_color(Color background, Color foreground)
{
    // this->background = background;
    // this->foreground = foreground;
}

void Widget::set_value(float value)
{ 
    this->value = value - (this->border_width * 2);
    if (this->value < 0.f) this->value = 0.f;
}

void Widget::set_pos(float left, float top)
{
    /*  */
    // if (this->is_containerized()) return;
    if (left >= 0.f) this->left = left;
    if (top >= 0.f) this->top = top;
}

void Widget::release()
{
    if (this->is_containerized()) return;
    this->wData->removeFromWidgetlist(this);
    delete this;
}

void Widget::drawValueRect(PrimitiveBatch<VPC>* p_prim_batch)
{
    if (this->getDrawLayout() == WidgetDrawLayout::HEIGHT)
    {
        p_prim_batch->DrawQuad(
            VPC(Vector3(this->left + this->border_width,                this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->border_width,                this->top + this->height - this->border_width - this->value, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->width - this->border_width,  this->top + this->height - this->border_width - this->value, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->width - this->border_width,  this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND))
        );
    } else if (this->getDrawLayout() == WidgetDrawLayout::WIDTH)
    {
        p_prim_batch->DrawQuad(
            VPC(Vector3(this->left + this->border_width,                    this->top + this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->border_width + this->value,      this->top + this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->border_width + this->value,      this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND)),
            VPC(Vector3(this->left + this->border_width,                    this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::FOREGROUND))
        );
    }
}

void Widget::handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch)
{
    if (keystate == KeyStates::PRESSED)
    {
        if (key == wData->get_bind(BindActions::LMBUTTON))
        {
            if (this->is_containerized()) return;
            this->is_moving = true;
        }

        if (key == wData->get_bind(BindActions::MMBUTTON))
        {
            if (this->is_containerized()) return;
            this->mode = (this->mode == WidgetMode::NORMAL) ? WidgetMode::EDITING : WidgetMode::NORMAL;
        }

        if (key == wData->get_bind(BindActions::RM_WIDGET))
        {
            if (!this->mode == WidgetMode::EDITING) return;
            for (Widget* widget: wData->widgetList->widgets)
            {
                if (widget == this)
                {
                    this->release();
                }
            }
        }

        if (key == wData->get_bind(BindActions::TOGGLE_DRAW_LAYOUT))
        {
            if (!this->mode == WidgetMode::EDITING) return;
            this->drawLayout = (this->drawLayout == WidgetDrawLayout::HEIGHT) ? WidgetDrawLayout::WIDTH : WidgetDrawLayout::HEIGHT;
        }

    } else if (keystate == KeyStates::RELEASED)
    {
        if (key == wData->get_bind(BindActions::LMBUTTON))
        {
            this->is_moving = false;

            WidgetContainer<Widget*>* widget = this->get_last_dropin();
            if (widget && widget->xy_inside(mouseX, mouseY))
            {
                widget->dropin_widget(this, mouseX, mouseY);
            }

            // XXX: can I improve this last_dropin system?
            // if (Widget* widgetCont = this->get_last_dropin())
            // {
            //     if (widgetCont == nullptr) return;
            //     if (!widgetCont->xy_inside(mouseX, mouseY))
            //     {
            //         this->set_last_dropin(nullptr);
            //         return;
            //     }
            //     widgetCont->dropin_widget(this, mouseX, mouseY);
            // }
        }
    }
}

void Widget::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    if (this->is_moving && !(this->is_containerized()))
    {
        this->set_pos(mouseX, mouseY);
        drawDropinOverlay(this, p_prim_batch, this->wData, mouseX, mouseY);
    }

    this->draw_outline(p_prim_batch, this->border_width, true);
    // p_prim_batch->DrawQuad(
    //     VPC(Vector3(this->left,                 this->top, this->z_plane), this->get_color(ColorType::BORDER_COLOR)),
    //     VPC(Vector3(this->left + this->width,   this->top, this->z_plane), this->get_color(ColorType::BORDER_COLOR)),
    //     VPC(Vector3(this->left + this->width,   this->top + this->height, this->z_plane), this->get_color(ColorType::BORDER_COLOR)),
    //     VPC(Vector3(this->left,                 this->top + this->height, this->z_plane), this->get_color(ColorType::BORDER_COLOR))
    // );
    p_prim_batch->DrawQuad(
        VPC(Vector3(this->left + this->border_width,                  this->top + this->border_width, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left + this->width - this->border_width,    this->top + this->border_width, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left + this->width - this->border_width,    this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left + this->border_width,                  this->top + this->height - this->border_width, this->z_plane), this->get_color(ColorType::BACKGROUND))
    );

    if (this->mode == WidgetMode::EDITING)
    {
        this->draw_outline(p_prim_batch);
    }
};

template<class T>
void WidgetContainer<T>::draw_dropin_overlay(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    if (this->layout == WidgetContainerLayout::HORIZONTAL)
    {
        float ov_left = this->left;
        float ov_width = this->width / 2;

        if ( (mouseX - this->left) > this->width / 2 )
        {
            ov_left += this->width / 2;
        };
        p_prim_batch->DrawQuad(
            VPC(Vector3(ov_left,                    this->top,                  this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(ov_left + ov_width,         this->top,                  this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(ov_left + ov_width,         this->top + this->height,   this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(ov_left,                    this->top + this->height,   this->z_plane), this->get_color(ColorType::OVERLAY))
        );
    } else if (this->layout == WidgetContainerLayout::VERTICAL)
    {
        float ov_top = this->top;
        float ov_height = this->height / 2;

        if ( (mouseY - this->height) > this->top / 2 )
        {
            ov_top += this->height / 2;
        };
        p_prim_batch->DrawQuad(
            VPC(Vector3(this->left,                    ov_top,                  this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(this->left + this->width,      ov_top,                  this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(this->left + this->width,      ov_top + ov_height,      this->z_plane), this->get_color(ColorType::OVERLAY)),
            VPC(Vector3(this->left,                    ov_top + ov_height,      this->z_plane), this->get_color(ColorType::OVERLAY))
        );
    }
}

template<class T>
WidgetContainer<T>::WidgetContainer()
{
    this->setID(uuid::generate_uuid_v4());
}

template<class T> 
WidgetContainer<T>::WidgetContainer(WidgetContainerLayout layout)
{
    this->setID(uuid::generate_uuid_v4());
    this->setLayout(layout); 
}

template<class T>
bool WidgetContainer<T>::has_childs()
{
    return this->childs.widgets.size() > 0 ? true: false;
}

template<class T>
void WidgetContainer<T>::setLayout(WidgetContainerLayout layout)
{
    this->layout = layout;
    this->update_child_positions();
}

template<class T>
void WidgetContainer<T>::handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch)
{
    if (keystate == KeyStates::PRESSED)
    {
        if (key == wData->get_bind(BindActions::LMBUTTON))
        {
            std::cout << "lmbutton: " << this->is_containerized() << std::endl;
            if (this->is_containerized()) return; 
            this->is_moving = true;
        }

        if (key == wData->get_bind(BindActions::MMBUTTON))
        {
            this->mode = (this->mode == WidgetMode::NORMAL) ? WidgetMode::EDITING : WidgetMode::NORMAL;
        }

        if (key == wData->get_bind(BindActions::TOGGLE_LAYOUT))
        {
            this->layout = (this->layout == WidgetContainerLayout::HORIZONTAL) ? WidgetContainerLayout::VERTICAL : WidgetContainerLayout::HORIZONTAL;
            this->update_child_positions();
        }
    }
    else if (keystate == KeyStates::RELEASED)
    {
        if (key == wData->get_bind(BindActions::LMBUTTON))
        {
            this->is_moving = false;
            WidgetContainer<Widget*>* widget = this->get_last_dropin();
            if (widget && widget->xy_inside(mouseX, mouseY))
            {
                widget->dropin_widget(this, mouseX, mouseY);
            }
        }
    }
}

template<class T>
int WidgetContainer<T>::get_dropin_index(float mouseX, float mouseY)
{
    if (this->layout == WidgetContainerLayout::HORIZONTAL)
    {
        if ( (mouseX - this->left) > this->width / 2 )
        {
            return this->childs.widgets.size();
        } else {
            return 0;
        }
    } else if (this->layout == WidgetContainerLayout::VERTICAL)
    {
        if ( (mouseY - this->height) > this->top / 2 )
        {
            return this->childs.widgets.size();
        } else {
            return 0;
        }
    }
    return 0;
}

template<class T>
void WidgetContainer<T>::dropin_widget(T widget, float mouseX, float mouseY)
{
    int index = this->get_dropin_index(mouseX, mouseY);
    this->insert_child(widget, index);
}

template<class T>
void WidgetContainer<T>::update_child_positions()
{
    if (!(this->has_childs())) return;

    if (this->layout == WidgetContainerLayout::HORIZONTAL)
    {
        float max_height = 0.f, sum_width = 0.f;
        for (int i=0; i < this->childs.widgets.size(); i++)
        {
            Widget* widget = this->childs.widgets[i];
            widget->set_pos(
                this->left + sum_width + (this->widget_padding * i) + this->border_width,
                this->top + this->border_width
            );
            sum_width += widget->width;
            if (widget->height > max_height) max_height = widget->height;
        }
        this->set_size(
            sum_width + (this->widget_padding * (this->childs.widgets.size() - 1)) + this->border_width * 2,
            max_height + (this->border_width * 2)
        );
    } else if (this->layout == WidgetContainerLayout::VERTICAL)
    {
        float sum_height = 0.f, max_width = 0.f;
        for (int i=0; i < this->childs.widgets.size(); i++)
        {
            Widget* widget = this->childs.widgets[i];
            widget->set_pos(
                this->left + this->border_width,
                this->top + sum_height + widget->border_width + (this->widget_padding * i)
            );
            sum_height += widget->height;
            if (widget->width > max_width) max_width = widget->width;
        }        
        this->set_size(
            max_width + (this->border_width * 2),
            sum_height + (this->widget_padding * (this->childs.widgets.size() - 1)) + this->border_width * 2
        );
    }
}

template<class T>
void WidgetContainer<T>::set_containerized(bool yesno)
{
    if (yesno)
    { // inside container
        this->border_width = 0.f;
        this->widget_is_containerized = yesno;
    } else 
    { // __not__ inside container
        this->border_width = 12.f;
        this->widget_is_containerized = yesno;
    }
}

template<class T>
void WidgetContainer<T>::insert_child(T widget, int index, bool force)
{
    if(widget->is_containerized()) return;
    widget->set_containerized(true);
    widget->setParentID(widget->getID());

    if (widget->getMode() == WidgetMode::EDITING)
    {
        widget->setMode(WidgetMode::NORMAL);
    }

    if (index >= 0)
    {
        auto it = this->childs.widgets.begin();
        this->childs.widgets.insert(it+index, widget);
    } else {
        // shouldn't call add_widget here? because when inserting another container
        // it would also add all childs of that container
        this->childs.widgets.insert(this->childs.widgets.end(), widget);
    }
    this->update_child_positions();
}

template<class T>
void WidgetContainer<T>::set_pos(float left, float top)
{
    Widget::set_pos(left, top);
    this->update_child_positions();
}

template<class T>
void WidgetContainer<T>::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    // if (!(this->has_childs())) return;
    if (this->is_moving) {
        this->set_pos(mouseX, mouseY);
        this->update_child_positions();
        if (!this->is_containerized())
        {
            drawDropinOverlay(this, p_prim_batch, this->wData, mouseX, mouseY);
        }
    }
    if (!(this->has_childs()))
    {
        p_prim_batch->DrawQuad(
            VPC(Vector3(this->left,                  this->top, this->z_plane), this->get_color(ColorType::BACKGROUND)),
            VPC(Vector3(this->left + this->width,    this->top, this->z_plane), this->get_color(ColorType::BACKGROUND)),
            VPC(Vector3(this->left + this->width,    this->top + this->height, this->z_plane), this->get_color(ColorType::BACKGROUND)),
            VPC(Vector3(this->left,                  this->top + this->height, this->z_plane), this->get_color(ColorType::BACKGROUND))
        );
    }

    for (Widget* widget: this->childs.widgets)
    {
        widget->draw(p_prim_batch, mouseX, mouseY);
    }

    if (this->mode == WidgetMode::EDITING)
    {
        this->draw_outline(p_prim_batch);
    }
    
}

template<class T>
XMFLOAT4 WidgetContainer<T>::get_color(ColorType type)
{
    if (this->colorMap.find(type) != this->colorMap.end())
    {
        return this->colorMap[type];
    } else {
        return Widget::get_color(type);
    }
}

template<class T>
void WidgetContainer<T>::update_value(ACCData* acc)
{
    for (Widget* widget: this->childs.widgets)
    {
        widget->update_value(acc);
    }
}

template<class T>
void WidgetSpawner<T>::update_child_positions()
{
    float sum_height = 0.f;
    for (int i=0; i < this->childs.widgets.size(); i++)
    {
        Widget* widget = this->childs.widgets[i];
        widget->set_pos(
            this->left + this->border_width,
            sum_height + this->top + this->border_width + (this->widget_padding * i)
        );
        sum_height += widget->height;
    }
}

template<class T>
WidgetSpawner<T>::WidgetSpawner(WidgetData* pWidgetData)
{
    D3D11_VIEWPORT* pViewport = pWidgetData->getViewport();
    this->pWidgetData = pWidgetData;
    this->setLayout(WidgetContainerLayout::VERTICAL);
    this->set_size(60.f, pViewport->Height);
    this->set_pos(pViewport->Width - this->width, 0.f);

    this->toolTipSprite = new SpriteHelper::FontSprite(L"", this->pWidgetData->getSpriteFont(), 0.35f);
    for (auto icon: this->available_widgets)
    { // second is the widgeticonresource, first is classname str
        this->insert_child(icon.second);
    }
}

template<class T>
void WidgetSpawner<T>::handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch)
{
    if (keystate == KeyStates::PRESSED)
    {
        if (key == wData->get_bind(BindActions::TOGGLE_WIDGETSPAWNER))
        {
            this->draw_me = this->draw_me ? false : true;
        }

        if (key == wData->get_bind(BindActions::LMBUTTON))
        {
            if (this->draw_me)
            {
                for (auto icon: this->childs.widgets)
                {
                    if (icon->xy_inside(wData->mousePos->x, wData->mousePos->y))
                    {
                        Widget* temp = icon->spawn_fn();
                        temp->wData = wData;
                        wData->widgetList->add_widget(temp);
                    }
                }
            }
        }

    }
}

template<class T>
XMFLOAT4 WidgetSpawner<T>::get_color(ColorType type)
{
    if (this->colorMap.find(type) != this->colorMap.end())
    {
        return this->colorMap[type];
    } else {
        return Widget::get_color(type);
    }
}

template<class T>
void WidgetSpawner<T>::draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY)
{
    if (!this->draw_me) return;

    p_prim_batch->DrawQuad(
        VPC(Vector3(this->left, this->top, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left + this->width, this->top, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left + this->width, this->top + this->height, this->z_plane), this->get_color(ColorType::BACKGROUND)),
        VPC(Vector3(this->left, this->top + this->height, this->z_plane), this->get_color(ColorType::BACKGROUND))
    );
    for (WidgetIconResource* icon: this->childs.widgets)
    {
        if (icon->xy_inside(mouseX, mouseY))
        {
            this->toolTipSprite->setOutputText(icon->tooltipText, this->pWidgetData->getSpriteFont());

            this->toolTipSprite->position = Vector2(
                icon->left - ((this->toolTipSprite->size.x / 2.f) * this->toolTipSprite->scale),
                icon->top + icon->height / 2.f
            );
            this->pWidgetData->queueFontSprite(this->toolTipSprite);
        }
        p_prim_batch->DrawQuad(
            VPC(Vector3(icon->left, icon->top, icon->z_plane), icon->icon_resource),
            VPC(Vector3(icon->left + icon->width, icon->top, icon->z_plane), icon->icon_resource),
            VPC(Vector3(icon->left + icon->width, icon->top + icon->height, icon->z_plane), icon->icon_resource),
            VPC(Vector3(icon->left, icon->top + icon->height, icon->z_plane), icon->icon_resource)
        );
    };
}

void WidgetData::removeFromWidgetlist(Widget* widget)
{
    for (int i = 0; i < this->widgetList->widgets.size(); i++)
    {
        Widget* item = this->widgetList->widgets[i];
        if (item == widget)
        {
            this->widgetList->widgets.erase(this->widgetList->widgets.begin() + i);
            return;
        }
    }
}

void WidgetData::updateWidgetList()
{
    this->toDrawWidgets.clear();
    for (Widget* widget: this->widgetList->widgets)
    {
        if (widget->get_type() == WidgetType::WIDGET)
        {
            this->toDrawWidgets.insert(this->toDrawWidgets.end(), widget);
        } else if (widget->get_type() == WidgetType::CONTAINER)
        {
            this->toDrawWidgets.insert(this->toDrawWidgets.begin(), widget);
        } else
        {
            this->toDrawWidgets.insert(this->toDrawWidgets.begin(), widget);
        } 
    }
}

void WidgetData::queueFontSprite(SpriteHelper::FontSprite* fontSprite)
{
    // TODO: index?
    this->toDrawFontSprites.push_back(fontSprite);
};

void WidgetData::drawFontSprites(DirectX::SpriteBatch* pSpriteBatch, DirectX::SpriteFont* pSpriteFont)
{
    for (SpriteHelper::FontSprite* fontSprite: this->toDrawFontSprites)
    {
        pSpriteFont->DrawString(
            pSpriteBatch,
            fontSprite->outputText.c_str(),
            fontSprite->position,
            fontSprite->color,
            fontSprite->rotation,
            fontOrigin,
            fontSprite->scale
        );
    }
    this->toDrawFontSprites.clear();
}

WidgetIconResource::WidgetIconResource(fn_spawn_self p_spawn_self, SimpleMath::Color icon_resource, wchar_t* tooltipText)
{
    this->set_size(40.f, 40.f);
    this->spawn_fn = p_spawn_self;
    this->icon_resource = icon_resource;
    this->tooltipText = tooltipText;
};

void ConfigHelper::write(std::string string, int indent)
{
    std::string ws = "";
    for (int i = 0; i < indent; i++)
    {
        ws += this->wsindent;
    }
    // std::cout << "writing indent(" << indent << ")" << ws << string << std::endl;
    this->file << ws << string << std::endl;
};

void ConfigHelper::writeWidgetData(std::string id, Widget* widget, int indent, std::string parentId = "")
{
    this->write("- " + widget->getClassName() + ":", indent);
    this->write("ID: " + id, indent + 1); 
    this->write("Top: " + std::to_string(widget->top), indent + 1);
    this->write("Left: " + std::to_string(widget->left), indent + 1);
    this->write("Height: " + std::to_string(widget->height), indent + 1);
    this->write("Width: " + std::to_string(widget->width), indent + 1);
    this->write("DrawLayout: " + std::to_string(widget->getDrawLayout()), indent + 1);
    if (parentId.size())
    {
        this->write("ParentID: " + parentId, indent + 1);
    }
}

void ConfigHelper::writeWidgetData(std::string id, WidgetContainer<Widget*>* widget, int indent, std::string parentId = "")
{
    std::cout << "beginIndent" << indent << std::endl;
    this->write("- " + widget->getClassName() + ":", indent);
    this->write("ID: " + id, indent + 1); 
    this->write("Top: " + std::to_string(widget->top), indent + 1);
    this->write("Left: " + std::to_string(widget->left), indent + 1);
    this->write("Layout: " + std::to_string(widget->getLayout()), indent + 1);
    if (parentId.size())
    {
        this->write("ParentID: " + parentId, indent + 1);
    }

    if (widget->has_childs())
    {
        this->write("Children:", indent + 1);
        for (Widget* childWidget: widget->childs.widgets)
        {
            if (childWidget->get_type() == WidgetType::CONTAINER)
            {
                WidgetContainer<Widget*>* childCont = dynamic_cast<WidgetContainer<Widget*>*>(childWidget);
                if (childCont)
                {
                    this->writeWidgetData(childCont->getID(), childCont, indent + 2, id);
                }
            } else {
                this->writeWidgetData(childWidget->getID(), childWidget, indent + 2, id);
            }
        }
    }
}

void ConfigHelper::saveConfig()
{
    this->file.open(this->configPath, std::ios::out);
    if (!this->file) return;

    this->write("---", 0);
    this->write("Widgets:", 0);

    for (Widget* widget: this->wData->widgetList->widgets)
    {
        // std::string id = uuid::generate_uuid_v4();
        if (widget->get_type() == WidgetType::WIDGET)
        {
            if (widget->is_containerized()) continue;
            this->writeWidgetData(widget->getID(), widget, 1);

        }
        if (widget->get_type() == WidgetType::CONTAINER)
        {
            WidgetContainer<Widget*>* widgetCont = dynamic_cast<WidgetContainer<Widget*>*>(widget);
            if (widgetCont->is_containerized()) continue;
            if (widgetCont)
            {
                this->writeWidgetData(widget->getID(), widgetCont, 1);
            }
        }
    }
    this->file.close();
};

void ConfigHelper::readConfig(WidgetList<Widget*>* widgetList)
{
    // If not called in WidgetData Constructor youll need to add wData to every widget manually
    std::ifstream file(this->configPath);
    bool foundWidget = false;
    int indentSize = this->wsindent.size();
    int expectedIndent = 0;
    int lineNr = 0;

    std::vector<std::string> available_widgets;
    for (auto s: this->wData->widgetSpawner->available_widgets)
    {
        // std::cout << "availableWidgets:" << s.first << std::endl;
        available_widgets.push_back(s.first);
    }

    std::map<std::string, Widget*> createdWidgets;
    Widget* workingWidget = nullptr;

    for (std::string line; std::getline(file, line);)
    {
        if (!line.size()) continue;
        lineNr ++;
        if (line == "Widgets:")
        {
            foundWidget = true;
            expectedIndent += indentSize;
            continue;
        }
        if (foundWidget)
        {
            bool found_non_ws = false;
            int wsLen = 0;
            std::string str = "";
            for (char c: line)
            {
                if (isspace(c) && !found_non_ws)
                {
                    wsLen++;
                }
                else
                {
                    found_non_ws = true;
                    str += c;
                }
            };

            if (!expectedIndent == wsLen)
            {
                if (!wsLen == indentSize)
                {
                    std::cout << " [hook] Config Error: Wrong indent at line (" << lineNr << ")" << std::endl;
                    break;
                } else
                {
                    expectedIndent = indentSize;
                }
            }

            // std::cout << "wsLen(" << wsLen << ")" << str << std::endl;
            str.erase(remove_if(str.begin(), str.end(), isspace), str.end());

            if (str.at(0) == '#') continue;
            if (str.at(0) == '-')
            {
                if (workingWidget)
                {
                    // std::cout << "insert" << std::endl;
                    createdWidgets.insert({workingWidget->getID(), workingWidget});
                    // std::cout << "after insert" << std::endl;
                    workingWidget = nullptr;
                }

                std::string::size_type pos_d = str.find(":");
                std::string parse = str.substr(1, pos_d - 1);
                if (std::find(available_widgets.begin(), available_widgets.end(), parse) != available_widgets.end())
                {
                    std::cout << "toParseWidget:" << parse << std::endl;
                    expectedIndent += indentSize;

                    WidgetIconResource* icon = this->wData->widgetSpawner->available_widgets[parse];
                    workingWidget = icon->spawn_fn();
                    std::cout << "workingWidget: " << workingWidget << std::endl;
                }
                else
                {
                    // std::cerr << " [hook] Config Error: Could not parse Widget '" << parse << "'" << std::endl;
                }
            }
            else
            {
                if (workingWidget == nullptr)
                {
                    // std::cerr << "workingWidget is null, continue" << std::endl;
                };

                std::string::size_type pos_d = str.find(":");
                if (pos_d)
                {
                    std::string value = str.substr(pos_d+1);
                    std::string prop = str.substr(0, str.size() - value.size() - 1);
                    // std::cout << "prop: " << prop << " value: " << value << " Widget: " << workingWidget << std::endl;

                    if (prop == "Top")
                    {
                        workingWidget->top = std::stof(value);
                    } else if (prop == "Left")
                    {
                        workingWidget->left = std::stof(value);
                    } else if (prop == "ID")
                    {
                        workingWidget->setID(value);
                    } else if (prop == "ParentID")
                    {
                        workingWidget->setParentID(value);
                    } else if (prop == "Height")
                    {
                        workingWidget->height = std::stof(value);
                    } else if (prop == "Width")
                    {
                        workingWidget->width = std::stof(value);
                    } else if (prop == "DrawLayout")
                    {
                        workingWidget->setDrawLayout((WidgetDrawLayout)std::stof(value));
                    }else if (prop == "Layout")
                    {
                        // conversion from std::string -> int -> WidgetContainerLayout
                        workingWidget->setLayout((WidgetContainerLayout)std::stof(value));
                    } else if (prop == "Children")
                    {
                        expectedIndent += indentSize;
                    }
                }
            }            
        }
    }

    if (workingWidget)
    {
        createdWidgets.insert({workingWidget->getID(), workingWidget});
    }

    for (auto x: createdWidgets)
    {
        Widget* widget = dynamic_cast<Widget*>(x.second);
        if (!widget) continue;

        if (!widget->getParentID().empty())
        {
            WidgetContainer<Widget*>* parentWidget = dynamic_cast<WidgetContainer<Widget*>*>(createdWidgets[widget->getParentID()]);
            // std::cout << "Adding: " << widget->getID() << " To: " << widget->getParentID() << std::endl;
            parentWidget->insert_child(widget);
        }
    }

    for (auto x: createdWidgets)
    {
        if (x.second->get_type() == WidgetType::CONTAINER)
        {
            WidgetContainer<Widget*>* cont = dynamic_cast<WidgetContainer<Widget*>*>(x.second);
            cont->update_child_positions();
        }
    }

    for (auto x: createdWidgets)
    {
        /*
        widgetList->add_widget() will add all children of containers to the widgetList recursively
        this needs to be avoided manually for now - thus skip all widgets with parentId
        */
        if (x.second->getParentID().size()) continue;
        widgetList->add_widget(x.second);
    }
}
