#pragma once
#include "acc.h"
#include "KeyTracker.h"
#include "uuid_gen.h"
#include "spriteHelper.h"
#include "utils.h"

#include <windows.h>
#include <windowsx.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>
#include <map>


#include <D3Dcompiler.h>
#include <SpriteFont.h>
#include <SpriteBatch.h>
#include <WICTextureLoader.h>
#include <VertexTypes.h>
#include <PrimitiveBatch.h>
#include <SimpleMath.h>
#include <DirectXColors.h>
#include <Effects.h>

// #include <d3dcompiler.h>
// #include <d3d11.h>
#include <d3dx11.h>
// #include <d3dx10.h>
#include <detours/detours.h>

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "DirectXTK.lib")

using namespace DirectX;
using namespace DirectX::SimpleMath;
using VPC = DirectX::VertexPositionColor;

template<class T>
struct WidgetList;
template<class T>
struct WidgetContainer;
template<class T>
struct WidgetSpawner;

struct Widget;
struct ThrottleWidget;
struct BrakeWidget;
struct WidgetIconResource;
struct WidgetData;
struct ConfigHelper;

static XMFLOAT4 colorFromRGBA(int r, int g, int b, float alpha = 255.0f)
{
    if (r > 255) r = 255;
    if (r < 0) r = 0;
    if (g > 255) g = 255;
    if (g < 0) g = 0;
    if (b > 255) b = 255;
    if (b < 0) b = 0;
    if (alpha > 255.0f) alpha = 255.0f;
    if (alpha < 0.0f) alpha = 0.0f;

    return XMFLOAT4(
        (float) r / 255.f,
        (float) g / 255.f,
        (float) b / 255.f,
        alpha
    );
};

enum WidgetDrawLayout
{
    HEIGHT,
    WIDTH
};

enum ColorType
{
    FOREGROUND,
    BACKGROUND,
    BORDER_COLOR,
    EDIT_MODE,
    EDIT_MODE_CHILD,
    OVERLAY,
};

enum BindActions
{
    LMBUTTON,
    MMBUTTON,
    TOGGLE_WIDGETSPAWNER,
    TOGGLE_LAYOUT,
    TOGGLE_DRAW_LAYOUT,
    RM_WIDGET,
    SAVE_CONFIG,
    LOAD_CONFIG,
};

enum WidgetContainerLayout
{
    VERTICAL,
    HORIZONTAL
};

enum WidgetMode
{
    EDITING,
    NORMAL
};

enum WidgetType
{
    WIDGET,
    LIST,
    DATA,
    SPAWNER,
    CONTAINER,
    SPAWNER_ICON_RES,
};

typedef Widget* (*fn_spawn_self)(void);
typedef std::map<BindActions, int> userBindsT;

struct Widget
{
public:
    Widget() = default;
    virtual void update_value(ACCData* acc);
    virtual void set_pos(float left, float top);
    virtual void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY);
    virtual void handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch);
    virtual void release();
    virtual void setLayout(WidgetContainerLayout layout) { };
    virtual WidgetContainerLayout getLayout() { return WidgetContainerLayout::HORIZONTAL; };
    virtual void setID(std::string id) { this->id = id; };
    virtual std::string getID() { return this->id; };
    virtual void setParentID(std::string id) { this->parentId = id; };
    virtual std::string getParentID() { return this->parentId; };
    virtual void set_containerized(bool yesno) { this->widget_is_containerized = yesno; };
    virtual bool is_containerized() { return this->widget_is_containerized; };
    virtual void insert_child(Widget* widget, int index = -1, bool force = false) { return; };
    virtual WidgetMode getMode() { return this->mode; };
    virtual void setMode(WidgetMode mode) { this->mode = mode; };
    virtual WidgetType get_type() {return WidgetType::WIDGET;};
    virtual XMFLOAT4 get_color(ColorType type);
    virtual std::string getClassName() { return "Widget"; };
    virtual WidgetDrawLayout getDrawLayout() { return this->drawLayout; };
    virtual void setDrawLayout(WidgetDrawLayout drawLayout) { this->drawLayout = drawLayout; };
    virtual void drawValueRect(PrimitiveBatch<VPC>* p_prim_batch);

    void draw_outline(PrimitiveBatch<VPC>* p_prim_batch, int line_width, bool drawAsBorder);
    void set_size(float width, float height);
    void set_value(float value);
    void set_color(Color background, Color foreground);
    bool xy_inside(float mx, float my);
    void set_last_dropin(WidgetContainer<Widget*>* widgetCont ) {this->last_dropin = widgetCont;};
    WidgetContainer<Widget*>* get_last_dropin(void) {return this->last_dropin;};

    bool is_active = false;
    bool is_moving = false;
    float left = 30.f, top = 30.f, width = 40.f, height = 200.f;    
    float border_width = 4.f;
    float z_plane = 0.0f;

    WidgetData* wData;
private:
    WidgetDrawLayout drawLayout = WidgetDrawLayout::HEIGHT;
    WidgetContainerLayout layout;
    std::string id;
    std::string parentId;
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(30, 30, 30)},
        {ColorType::FOREGROUND, colorFromRGBA(100, 100, 100)},
        {ColorType::BORDER_COLOR, colorFromRGBA(0, 0, 0)},
        {ColorType::EDIT_MODE, colorFromRGBA(227, 155, 20)},
        {ColorType::EDIT_MODE_CHILD, colorFromRGBA(222, 110, 95)},
        {ColorType::OVERLAY, colorFromRGBA(252, 226, 116, 100)}
    };

    WidgetMode mode = WidgetMode::NORMAL;
    WidgetContainer<Widget*>* last_dropin = nullptr;
    float value = 40.f;
    bool widget_is_containerized = false;
};

struct GearWidget: Widget
{
public:
    GearWidget() { this->setID(uuid::generate_uuid_v4()); };
    static Widget* spawn_self(){ return new GearWidget(); };
    static wchar_t* getTooltipText() { return L"Gear"; };
    std::string getClassName() { return "GearWidget"; };
    void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY) override;
    void update_value(ACCData* acc) override;
    XMFLOAT4 get_color(ColorType type);

    float width = 40.0f, height = 40.0f;
    SpriteHelper::FontSprite* gearSprite = new SpriteHelper::FontSprite(1.0f);
private:
    std::wstring value;
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(0, 0, 0, 0.0f)},
        {ColorType::FOREGROUND, colorFromRGBA(124, 230, 100)},
        {ColorType::BORDER_COLOR, colorFromRGBA(0, 0, 0, 0.0f)},
    };
};


struct ThrottleWidget: Widget
{
public:
    ThrottleWidget();
    static Widget* spawn_self(){ return new ThrottleWidget();};
    static wchar_t* getTooltipText() { return L"Throttle"; };
    std::string getClassName() { return "ThrottleWidget"; };
    void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY) override;
    void update_value(ACCData* acc) override;
    XMFLOAT4 get_color(ColorType type);

    const wchar_t* tcSpriteText = L"TC:";
    SpriteHelper::FontSprite* tcSprite = new SpriteHelper::FontSprite(0.3f);
private:
    float value;
    WidgetDrawLayout drawLayout = WidgetDrawLayout::HEIGHT;
    std::string id;
    std::string parentId;
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(38, 173, 14)},
        {ColorType::FOREGROUND, colorFromRGBA(32, 212, 0)},
        {ColorType::BORDER_COLOR, colorFromRGBA(0, 0, 0)},
    };
}; 

struct BrakeWidget: Widget 
{
public:
    BrakeWidget();
    static Widget* spawn_self(){ return new BrakeWidget(); };
    static wchar_t* getTooltipText() { return L"Break"; };
    void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY) override;
    std::string getClassName() { return "BrakeWidget"; };
    void update_value(ACCData* acc) override;
    XMFLOAT4 get_color(ColorType type);

    const wchar_t* absSpriteText = L"ABS:";
    SpriteHelper::FontSprite* absSprite = new SpriteHelper::FontSprite(0.3f);
private:
    float value;
    WidgetDrawLayout drawLayout = WidgetDrawLayout::HEIGHT;
    std::string id;
    std::string parentId;
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(97, 0, 0)},
        {ColorType::FOREGROUND, colorFromRGBA(222, 27, 27)},
        {ColorType::BORDER_COLOR, colorFromRGBA(0, 0, 0)},
    };
};

template <class T>
struct WidgetList: std::vector<T>
{
public:
    WidgetType get_type() {return WidgetType::LIST;};
    std::vector<T> widgets;
    void add_widget(T widget);
    bool moving_widget_inside = false;
};

template <class T>
struct WidgetContainer: Widget
{
public:
    WidgetContainer();
    WidgetContainer(WidgetContainerLayout layout);
    static Widget* spawn_self() {return new WidgetContainer<Widget*>();};
    static wchar_t* getTooltipText() { return L"Container"; };
    virtual void set_pos(float left, float top);
    virtual void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY) override;
    virtual void update_child_positions();
    void setLayout(WidgetContainerLayout layout) override;
    WidgetContainerLayout getLayout() override { return this->layout; };
    void draw_dropin_overlay(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY);
    void dropin_widget(T widget, float mouseX, float mouseY);
    void handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch) override;
    void insert_child(T widget, int index = -1, bool force = false);
    void update_value(ACCData* acc);
    std::string getClassName() { return "WidgetContainer"; };

    XMFLOAT4 get_color(ColorType type);
    WidgetType get_type() {return WidgetType::CONTAINER;};
    int get_dropin_index(float mouseX, float mouseY);

    void set_containerized(bool yesno);
    bool is_containerized() { return this->widget_is_containerized; };
    bool has_childs();

    WidgetList<T> childs;
    const float widget_padding = 12.f;

private:
    std::string id;
    std::string parentId;
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(255, 255, 255, 200.0f)},
        {ColorType::FOREGROUND, colorFromRGBA(100, 100, 100)},
        {ColorType::EDIT_MODE, colorFromRGBA(227, 155, 20)},
        {ColorType::EDIT_MODE_CHILD, colorFromRGBA(222, 110, 95)},
        {ColorType::OVERLAY, colorFromRGBA(252, 226, 116, 100.0f)}
    };

    bool widget_is_containerized = false;
    Vector2 lastMouseXY = Vector2(0.f, 0.f);
    WidgetMode mode = WidgetMode::NORMAL;
    WidgetContainerLayout layout = WidgetContainerLayout::HORIZONTAL;
};

struct WidgetIconResource: Widget
{
    WidgetIconResource(fn_spawn_self p_spawn_self, SimpleMath::Color icon_resource, wchar_t* tooltipText);
    WidgetType get_type() {return WidgetType::SPAWNER_ICON_RES;};
    fn_spawn_self spawn_fn;
    Color icon_resource;
    wchar_t* tooltipText;
};

template<class T>
struct WidgetSpawner: WidgetContainer<T>
{
public:
    WidgetSpawner(WidgetData* pWidgetData);
    void draw(PrimitiveBatch<VPC>* p_prim_batch, float mouseX, float mouseY) override;
    void setLayout(WidgetContainerLayout layout) override {return;};
    void handle_inputs(WidgetData* wData, KeyStates keystate, int key, float mouseX, float mouseY, PrimitiveBatch<VPC>* p_prim_batch) override;
    void update_child_positions() override;
    WidgetType get_type() {return WidgetType::SPAWNER;};
    XMFLOAT4 get_color(ColorType type);

    SpriteHelper::FontSprite* toolTipSprite;

    bool draw_me = false;
    std::map<std::string, WidgetIconResource*> available_widgets = {
        {WidgetContainer().getClassName(), new WidgetIconResource {
            WidgetContainer<Widget*>::spawn_self, (SimpleMath::Color)Colors::White, WidgetContainer::getTooltipText()
            }},
        {ThrottleWidget().getClassName(), new WidgetIconResource {
            ThrottleWidget::spawn_self, (SimpleMath::Color)Colors::Green, ThrottleWidget::getTooltipText()
            }},
        {BrakeWidget().getClassName(), new WidgetIconResource {
            BrakeWidget::spawn_self, (SimpleMath::Color)Colors::Red, BrakeWidget::getTooltipText()
            }},
        {GearWidget().getClassName(), new WidgetIconResource {
            GearWidget::spawn_self, (SimpleMath::Color)Colors::Purple, GearWidget::getTooltipText()
            }}
    };
private:
    std::map<ColorType, XMFLOAT4> colorMap = {
        {ColorType::BACKGROUND, colorFromRGBA(80, 80, 80, 200.0f)},
    };
    const float border_width = 10.f;
    WidgetContainerLayout layout = WidgetContainerLayout::VERTICAL;
    WidgetData* pWidgetData;
};

struct WidgetData 
{
public:
    WidgetData(ACCData* acc, D3D11_VIEWPORT* pViewPort, DirectX::SpriteFont* g_pSpriteFont);
    void handleUserInputs(DirectX::PrimitiveBatch<VPC>* p_prim_batch);
    int get_bind(BindActions action);
    void removeFromWidgetlist(Widget* widget);
    void updateWidgetList();
    D3D11_VIEWPORT* getViewport() { return this->pViewport; };
    DirectX::SpriteFont* getSpriteFont() { return this->pSpriteFont; };
    ACCData* getACC() { return this->acc; };
    WidgetType get_type() {return WidgetType::DATA;};

    void queueFontSprite(SpriteHelper::FontSprite* fontSprite);
    void drawFontSprites(DirectX::SpriteBatch* pSpriteBatch, DirectX::SpriteFont* pSpriteFont);
    void queueTextureSprite(SpriteHelper::TextureSprite* textureSprite);
    
    WidgetList<Widget*>* widgetList;
    WidgetSpawner<WidgetIconResource*>* widgetSpawner;
    KeyTracker* pKeyTracker;
    ConfigHelper* configHelper;
    MousePos* mousePos;
    std::vector<Widget*> toDrawWidgets;

    std::vector<SpriteHelper::FontSprite*> toDrawFontSprites;
    std::vector<SpriteHelper::TextureSprite*> toDrawTextureSprites;
    DirectX::SpriteFont* pSpriteFont;

private:
    ACCData* acc;
    D3D11_VIEWPORT* pViewport;
    userBindsT userBinds;
    std::vector<Widget*> lastPressedWidgets;
};

struct ConfigHelper
{
    ConfigHelper(WidgetData* wData) { this->wData = wData; };
    void saveConfig();
    void write(std::string string, int indent = 1);
    void writeWidgetData(std::string id, Widget* widget, int indent, std::string parentId);
    void writeWidgetData(std::string id, WidgetContainer<Widget*>* widget, int indent, std::string parentId);
    void readConfig(WidgetList<Widget*>* widgetList);

    char* configPath = "C:\\Users\\tost\\config.yaml";
    
private:
    std::fstream file;
    std::string wsindent = "  ";
    WidgetData* wData;
};