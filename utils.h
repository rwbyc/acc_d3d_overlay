#pragma once
#include "acc_elements.h"
#include "acc.h"
#include <iostream>
#include <string>

namespace utils
{
    std::wstring addValueToWString(const wchar_t* str, int value);
    std::wstring addValueToWString(const wchar_t* str, float value);
};