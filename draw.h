#pragma once
#include "acc_elements.h"

#define safe_release(p) if (p) { p->Release(); p = nullptr; }

typedef HRESULT(__fastcall *IDXGISwapChainPresent)(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags);

void Log(LPCWSTR text);
void Log(float text);
void SetupWidgets(void);
bool getViewport(HWND hWnd);
bool mouseInWidget(float mx, float my, Widget widget);
void HandleUserInputs(DirectX::PrimitiveBatch<VPC>* p_prim_batch);
void SetupDrawQuad(ID3D11Device* pDevice, HWND hWnd);
void DrawQuad(ID3D11DeviceContext* pContext, ID3D11Device* pDevice);
int WINAPI d3dInit();

using VPC = DirectX::VertexPositionColor;

