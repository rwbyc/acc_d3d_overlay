#pragma once
#include <windows.h>
#include <vector>
#include <map>


enum KeyStates
{
    PRESSED,
    RELEASED,
    IDLE
};

struct KeyTracker
{
public:
    void track_button(int vk_code)
    {
        std::vector<int> key = {vk_code};
        if (this->key_mapping.find(key) != this->key_mapping.end())
        {
            this->key_mapping[key] = KeyStates::IDLE;
        }
    };

    bool pressed(int vk_code)
    {
        std::vector<int> key = {vk_code};
        if (this->key_mapping[key] == KeyStates::PRESSED)
        {
            return false;
        } else if (GetAsyncKeyState(vk_code) & 0x8000 && this->key_mapping[key] == KeyStates::IDLE)
        {
            this->key_mapping[key] = KeyStates::PRESSED;
            return true;
        }
        return false;
    };

    bool released(int vk_code)
    {
        std::vector<int> key = {vk_code};
        if (GetAsyncKeyState(vk_code) == 0 && this->key_mapping[key] == KeyStates::PRESSED)
        {
            this->key_mapping[key] = KeyStates::RELEASED;
            return true;
        } else if (this->key_mapping[key] == KeyStates::RELEASED)
        {
            this->key_mapping[key] = KeyStates::IDLE;
            return false;
        }
        return false;
    };

    KeyStates get_state(int vk_code)
    {
        std::vector<int> key = {vk_code};
        try
        {
            return this->key_mapping[key];
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            return KeyStates::IDLE;
        }
         
    }

private:
    std::map<std::vector<int>, KeyStates> key_mapping;
};


struct MousePos
{
public:
    void update_pos()
    {
        GetCursorPos(&this->curPos);
        this->x = this->curPos.x;
        this->y = this->curPos.y;
    };

    float x, y;

private:
    POINT curPos;
};
