#pragma once
#include <iostream>
#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>
#include <vector>

PROCESSENTRY32 entry;
HANDLE hWnd, hProc, AllocMem, remoteProcess, hKernel;
DWORD tExitCode;

std::vector<DWORD> PossiblePIDS;

const char path[] = "C:\\test.dll";
// const char processname[] = "RocketLeague.exe";
const char processname[] = "AC2-Win64-Shipping.exe";
// const char processname[] = "acc.exe";

void setUserDebugPrivileges() {
	HANDLE hProc, hToken;
	TOKEN_PRIVILEGES priv;
	LUID luid;

	hProc = GetCurrentProcess();
	OpenProcessToken(hProc, TOKEN_ADJUST_PRIVILEGES, &hToken);
	LookupPrivilegeValueW(0, L"setDebugPrivileges", &luid);

	priv.PrivilegeCount = 1;
	priv.Privileges[0].Luid = luid;
	priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	AdjustTokenPrivileges(hToken, false, &priv, 0, 0, 0);

	CloseHandle(hToken);
	CloseHandle(hProc);
}

bool findProcessesByName(const char* processname)
{    
    entry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32First(snapshot, &entry) == TRUE)
    {
        while (Process32Next(snapshot, &entry) == TRUE)
        {
			if (strcmp(entry.szExeFile, processname) == 0)
			{
				PossiblePIDS.push_back(entry.th32ProcessID);
			}
        }
    }
    CloseHandle(snapshot);
	if (PossiblePIDS.size() > 0){
		return true;
	} else {
    	return false;
	}
}

int main()
{
	setUserDebugPrivileges();

	std::wcout << "Looking for Process: '" << processname << "'" << std::endl;

	AllocMem = 0;
	int stop = 0;
	while(stop == 0)
	{
		if (findProcessesByName(processname))
		{
			Sleep(6000);
			// Sleep(20000);
			for (DWORD pid: PossiblePIDS)
			{
				hProc = OpenProcess(PROCESS_ALL_ACCESS, 0, pid);
				AllocMem = VirtualAllocEx(hProc, 0, sizeof(path), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
				std::wcout << AllocMem << std::endl;

				std::wcout << "Found process: " << processname << "PID: " << pid << std::endl;

				std::wcout << "Writing DLL Path" << std::endl;
				WriteProcessMemory(hProc, AllocMem, path, sizeof(path), 0);
				hKernel = GetModuleHandleW(L"kernel32.dll");

				if (!hKernel) { std::cout << "ERR 2" << std::endl; exit(2); }

				FARPROC routine = GetProcAddress((HMODULE)hKernel, "LoadLibraryA");
				remoteProcess = CreateRemoteThread(hProc, 0, sizeof(path), reinterpret_cast<LPTHREAD_START_ROUTINE>(routine), AllocMem, 0, 0);

				if (remoteProcess == 0) { std::cout << "Hit CreateRemoteThread: " << GetLastError() << std::endl; exit(3); }

				WaitForSingleObject(remoteProcess, INFINITE);
				GetExitCodeThread(remoteProcess, &tExitCode);

				if (tExitCode != 0) {
					std::cout << "DLL loaded successfully." << std::endl;
					VirtualFreeEx(hProc, AllocMem, sizeof(path), MEM_DECOMMIT);
					CloseHandle(hProc);
					stop = 1;
					break;
				} else {
					std::cout << "DLL load failed." << std::endl;
				}

				VirtualFreeEx(hProc, AllocMem, sizeof(path), MEM_DECOMMIT);
				CloseHandle(hProc);
			}
		}
	};
}