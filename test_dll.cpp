#pragma once
#define _MBCS
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include "draw.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD callReason, LPVOID lpReserved)
{
	// TODO: don't DisableThreadLibraryCalls and hook/unhook here?
	DisableThreadLibraryCalls(hModule);
    // DLL_{PROCESS|THREAD}_{DE|A}TTACH
	if( callReason == DLL_PROCESS_ATTACH)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)d3dInit, 0, 0, 0);
		// while (true)
		// {
			// if (GetAsyncKeyState(VK_END)) CreateThread(0, 0, (LPTHREAD_START_ROUTINE)unhook, 0, 0, 0);
			// if (GetAsyncKeyState(VK_HOME)) CreateThread(0, 0, (LPTHREAD_START_ROUTINE)hook, 0, 0, 0);
		// };
		// CreateThread(0, 0, (LPTHREAD_START_ROUTINE)d3dInit, 0, 0, 0);
	}
	return TRUE;
}