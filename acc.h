#pragma once
#include "AccSharedFileOut_1_6.h"
#include "acc.h"
#include "stdlib.h"
#include <windows.h>
#include <tchar.h>
#include <iostream>

struct SMElement
{
    HANDLE map_file;
    unsigned char* map_file_buff;
    LPCWSTR szName;
};


class ACCData
{
public:
    
    SMElement m_physics;
    SMElement m_graphics;
    SMElement m_static;

    SPageFilePhysics* pf_physics;
    SPageFileGraphic* pf_graphics;
    SPageFileStatic* pf_static;

    ACCData();

    float getThrottle(){
        return pf_physics->gas;
    };

    int currentGear()
    {
        return pf_physics->gear;
    };

    int getABS()
    {
        return pf_graphics->ABS;
    }

    int getTC()
    {
        return pf_graphics->TC;
    }

    float getBrake(){
        return pf_physics->brake;
    };

    float getFuel(){
        return pf_physics->fuel;
    };

    float getFuelEstimatedLaps() { return 0.f; };
    float getFuelXLap() {
        return pf_graphics->fuelXLap;
    };

    float getExhausTemperature(){
        return pf_graphics->exhaustTemperature;
    }
};

