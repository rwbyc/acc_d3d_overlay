#include "utils.h"

std::wstring utils::addValueToWString(const wchar_t* str, int value)
{
    return str + std::to_wstring(value);
};

std::wstring utils::addValueToWString(const wchar_t* str, float value)
{
    return str + std::to_wstring(value);
};