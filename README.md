## Setting up the Environment

### Required Downloads
Requirements:

1. the visual studio c++ build tools thingy
2. [Install DirectXSDK 2010(June)](https://www.microsoft.com/en-us/download/details.aspx?id=6812)
3. [Download the precompiled DirectXTK](https://drive.google.com/file/d/1nMvepct3P0SxPyCLEd99Qi-zXkaVK7HV/view)
4. [Install Detours using Microsoft VCPKG](https://github.com/microsoft/vcpkg#quick-start-windows)
```cmd
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg.exe install detours:x64-windows
```

Create file called `env.bat` with the content and adjust paths, this file is also in the .gitignore
```cmd
REM FILE: env.bat
@echo off

set VCPKG_INC="F:/l/vcpkg/installed/x86-windows/include"
set DIRECTXTK_INC="F:/l/DirectXTK/Inc"
set DIRECTXSDK_INC="C:/Program Files (x86)/Microsoft DirectX SDK (June 2010)/Include"

set VCPKG_LIB="F:/l/vcpkg/installed/x64-windows/lib"
set DIRECTXTK_LIB="F:/l/DirectXTK/Bin/Desktop_2019_Win10/x64/Release"
set DIRECTXSDK_LIB="C:/Program Files (x86)/Microsoft DirectX SDK (June 2010)/Lib/x64"
```

### Setting up vscode
Install c++ Extension and add this to your `settings.json`
```json
{
    "terminal.integrated.shell.windows": "cmd.exe",
    "terminal.integrated.shellArgs.windows": [
        "/k",
        "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\VC\\Auxiliary\\Build\\vcvars64.bat"
    ],
    "C_Cpp.default.includePath": [
        "${SCITERSDK}/include", "${default}", "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Include",
        "F:\\l\\DirectXTK\\Inc", "F:\\l\\vcpkg\\installed\\x86-windows\\include"
    ]
}
```

## Compiling
run this in the vscode terminal
```
compile.bat && start.bat
```