#pragma once
#include "draw.h"

#include <CommonStates.h>
#include <DirectXHelpers.h>
/*
FIX: 
[] Fix crash when hooked and changing resolution
    IDEA: hook SwapChain->ResizeBuffers and
        Either 1: change our viewport to the new size
        2: delete our viewport, call orig fn, create viewport again
        3: idea is shit?. (//w\\)
        4: remember last viewport if it changed, do the setup again?

TODO:
// General
[] Way to unhook and rehook
[/] Get SwapChain->Present Offset from VTables
[] Add more widgets
[] Fix machine specific paths and create installer (use %appdata%?)
[] Setup File Logging
[] *** update values when watching demo and online spectating
[] scrolling output console? e.g. an array of four FontSprites (so four lines) for general feedback, started, config loaded|saved, 

[] ** Widget Resizing
    [] widget dimensions into virtual functions
    [] widget min size based (static or based on font)

[] fix mouse anchor position
[] wrapper for PrimitiveBatch->DrawQuad so that I can controll the order, e.g. always draw moving elements and overlay on top
    e.g. something like SpriteHelper::* for widgets, then instead of calling drawQuad directly implement queue.

[] put colormap in config
[] put keybinds in config?

[] WidgetSpawner
    [] do the icons
        [] implement SpriteHelper::TextureSprite for the Icons

[] WidgetContainer
    [] parent size doesn't update when changing child container 
    [] be able to change order of widgets in container
    [] be able to remove widgets from container
*/

DirectX::PrimitiveBatch<VPC>* p_prim_batch;
DirectX::BasicEffect* p_effect;

std::unique_ptr<DirectX::SpriteBatch> g_pSpriteBatch;
std::unique_ptr<DirectX::SpriteFont> g_pSpriteFont;
std::unique_ptr<DirectX::CommonStates> g_pStates;
ACCData acc;
WidgetData* pWidgetData;

Matrix proj_matrix;
void const* pShaderByteCode;
size_t pShaderByteCodeLength;

BOOL g_once = true;
HWND window = NULL;
DXGI_SWAP_CHAIN_DESC scd;
DWORD_PTR* pSwapChainVtable;

IDXGISwapChain* pSwapChain = NULL;
IDXGISwapChainPresent fnIDXGISwapChainPresent;
ID3D11DeviceContext* pContext = NULL;
ID3D11Device* pDevice = NULL;
ID3D11RenderTargetView* pRenderTargetView;
ID3D11PixelShader* pPS;
ID3D11VertexShader* pVS;
ID3D11Buffer* pVBuffer;
ID3D11Buffer* pConstantBuffer;
ID3D11InputLayout* pLayout;
ID3D10Blob* pShaderBlob;
ID3D11SamplerState* pSamplerState;
D3D11_VIEWPORT pViewports[32];
D3D11_VIEWPORT* pViewport;

void SetupWidgets(void)
{
    pWidgetData = new WidgetData(&acc, pViewport, g_pSpriteFont.get());
}

void SetupDrawQuad(ID3D11Device* pDevice, ID3D11DeviceContext* pContext, HWND hWnd)
{
    p_effect = new DirectX::BasicEffect(pDevice);
    p_prim_batch = new DirectX::PrimitiveBatch<VPC>(pContext);

    g_pStates = std::make_unique<DirectX::CommonStates>(pDevice);
    g_pSpriteBatch = std::make_unique<DirectX::SpriteBatch>(pContext);
    g_pSpriteFont = std::make_unique<DirectX::SpriteFont>(pDevice, L"C:\\fira_mono.spritefont");

    proj_matrix = Matrix::CreateOrthographicOffCenter(
        0.f, float(pViewport->Width), float(pViewport->Height), 0.f, 0.f, 1.f
    );

    p_effect->SetProjection(proj_matrix);
    p_effect->SetVertexColorEnabled(true);
    CreateInputLayoutFromEffect<VPC>(pDevice, p_effect, &pLayout);
    pSamplerState = g_pStates->LinearClamp();
}

void DrawQuad(ID3D11DeviceContext* pContext, ID3D11Device* pDevice)
{
    pContext->PSSetSamplers(0, 1, &pSamplerState);
    pContext->IASetInputLayout(pLayout);
    p_effect->Apply(pContext);
    
    p_prim_batch->Begin();
    pWidgetData->updateWidgetList();
    for (Widget* widget: pWidgetData->toDrawWidgets)
    // for (Widget* widget: pWidgetData->widgetList->widgets)
    {
        if (widget->is_containerized()) continue;
        widget->update_value(&acc);
        widget->draw(p_prim_batch, pWidgetData->mousePos->x, pWidgetData->mousePos->y);
    };
    p_prim_batch->End();

    g_pSpriteBatch->Begin( SpriteSortMode_Deferred, g_pStates->NonPremultiplied() );
    pWidgetData->drawFontSprites(g_pSpriteBatch.get(), g_pSpriteFont.get());
    g_pSpriteBatch->End();
}

HRESULT __fastcall HkSwapChainPresent(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags)
{
    if (g_once) 
    {
        if (SUCCEEDED(pSwapChain->GetDevice(__uuidof(ID3D11Device), (void**)&pDevice)))
		{
			pDevice->GetImmediateContext(&pContext);
			DXGI_SWAP_CHAIN_DESC sd;
			pSwapChain->GetDesc(&sd);
			window = sd.OutputWindow;
			ID3D11Texture2D* pBackBuffer;
			pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
			pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);
			pBackBuffer->Release();
			// oWndProc = (WNDPROC)SetWindowLongPtr(window, GWLP_WNDPROC, (LONG_PTR)WndProc);
			g_once = false;

            if (getViewport(window))
            {
                Log(L"Setting up Widgets!");
                SetupDrawQuad(pDevice, pContext, window);
                SetupWidgets();
            } else 
            {
                Log(L"Could not get Viewport!");
            }
        } else 
        {
            return fnIDXGISwapChainPresent(pSwapChain, SyncInterval, Flags);
        }
        g_once = false;
        Log(L"HkSwapChainPresent END G_ONCE");

    }
    pContext->OMSetRenderTargets(1, &pRenderTargetView, NULL);

    pWidgetData->handleUserInputs(p_prim_batch);
    DrawQuad(pContext, pDevice);

    return fnIDXGISwapChainPresent(pSwapChain, SyncInterval, Flags);
}

void ConsoleSetup()
{
	AllocConsole();
	SetConsoleTitleW(L"Console");
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
	freopen("CONIN$", "r", stdin);
}

int WINAPI d3dInit()
{
    ConsoleSetup();
    HWND hWnd = GetForegroundWindow();

    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
    DXGI_SWAP_CHAIN_DESC tempSwapChainDesc;
    ZeroMemory(&tempSwapChainDesc, sizeof(tempSwapChainDesc));
    tempSwapChainDesc.BufferCount = 1;
    tempSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tempSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    tempSwapChainDesc.OutputWindow = hWnd;
    tempSwapChainDesc.SampleDesc.Count = 1;
    tempSwapChainDesc.Windowed = TRUE;
    tempSwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    tempSwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    tempSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, &featureLevel, 1,
        D3D11_SDK_VERSION, &tempSwapChainDesc, &pSwapChain, &pDevice, NULL, &pContext)))
    {
        MessageBoxW(hWnd, L"Failed to create directX device and swapchain!", L"uBoos?", MB_ICONERROR);
        return NULL;
    }
    pSwapChainVtable = (DWORD_PTR*)pSwapChain;
    pSwapChainVtable = (DWORD_PTR*)pSwapChainVtable[0];

    fnIDXGISwapChainPresent = (IDXGISwapChainPresent)((PBYTE)pSwapChainVtable[8]);

    std::wcout << L"  [hook] Present Addr: " << std::hex << fnIDXGISwapChainPresent << std::endl;

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(&(LPVOID&)fnIDXGISwapChainPresent, (PBYTE)HkSwapChainPresent); 
    DetourTransactionCommit();

    pSwapChain->Release();
    pDevice->Release();

    return 0;
};

void Log(LPCWSTR text)
{
    std::wcout << L"  [hook]" << text << std::endl;
}

void Log(float text)
{
    std::wcout << L"  [hook]" << text << std::endl;
}

bool getViewport(HWND hWnd)
{
	UINT numViewports = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE;
    RECT windowRect;

    // Apparently this isn't universal. Works on most games
	pContext->RSGetViewports(&numViewports, pViewports);

	if (!numViewports || !pViewports[0].Width)
	{
        Log(L"Couldn't get viewport");
		if (!GetClientRect(hWnd, &windowRect))
            Log(L"Couldn't get windowRect");
			return false;
		Log(L"Making own viewport");
		pViewport->Width = windowRect.right;
		pViewport->Height = windowRect.bottom;
		pViewport->TopLeftX = windowRect.left;
		pViewport->TopLeftY = windowRect.top;
		pViewport->MinDepth = 0.0f;
		pViewport->MaxDepth = 1.0f;
	} else {
        Log(L"Found Viewport trying to set");
		pViewport = &pViewports[0];
	}
    std::cout << "  [hook]Viewport | Width: " << pViewport->Width << " Height: " << pViewport->Height << " TopLeftX: " << pViewport->TopLeftX << " TopLeftX: " << pViewport->TopLeftY << std::endl;
	return true;
}
