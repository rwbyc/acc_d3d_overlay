@echo off
set /a colour=%random% %%9 + 1
color 0%colour%
cls

call env.bat
set BUILDFOLDER=build\

if not exist %BUILDFOLDER% mkdir %BUILDFOLDER%
del /q %BUILDFOLDER%*

echo ************** INJ **************
cl.exe /MP /Fo%BUILDFOLDER% -I /O2 /GA /EHsc inj.cpp /link /SUBSYSTEM:CONSOLE Advapi32.lib User32.lib /out:%BUILDFOLDER%inj.exe

echo ************** DLL **************
cl.exe /Fo%BUILDFOLDER% /MP /MD /EHsc /O2 /std:c++17 ^
-I %VCPKG_INC% ^
-I %DIRECTXTK_INC% ^
-I %DIRECTXSDK_INC% ^
test_dll.cpp draw.cpp acc_elements.cpp acc.cpp spriteHelper.cpp utils.cpp /link /DLL ^
/LIBPATH:%DIRECTXSDK_LIB% ^
/LIBPATH:%DIRECTXTK_LIB% ^
/LIBPATH:%VCPKG_LIB% ^
User32.lib detours.lib DirectXTK.lib /OUT:%BUILDFOLDER%test.dll

copy %BUILDFOLDER%test.dll C:\test.dll /B /Y