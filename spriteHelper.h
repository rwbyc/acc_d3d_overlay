#pragma once
#include <iostream>
#include <WICTextureLoader.h>
#include <SpriteFont.h>
#include <SpriteBatch.h>
#include <SimpleMath.h>
#include <wrl\client.h>

#define PI 3.14159265358979323846
using namespace Microsoft::WRL;
using namespace DirectX::SimpleMath;

namespace SpriteHelper
{
    float angleDegreeFromRad(float radians);
    Vector2 getFontSize(DirectX::SpriteFont* pSpriteFont, std::wstring outputText, float scale);
    Vector2 getFontOrigin(DirectX::SpriteFont* pSpriteFont, std::wstring outputText, float scale);

    struct FontSprite
    {
        FontSprite(float scale);
        FontSprite(std::wstring outputText, DirectX::SpriteFont* pSpriteFont, float scale);
        void setOutputText(std::wstring outputText, DirectX::SpriteFont* pSpriteFont);
        Vector2 getFontOrigin(DirectX::SpriteFont* pSpriteFont);
        Vector2 getFontSize(DirectX::SpriteFont* pSpriteFont);

        std::wstring outputText;
        float rotation = 0.0f; // in radians
        float scale = 1.0f;
        Vector2 position = Vector2(100.f, 100.f);
        Vector2 origin = Vector2(0.f, 0.f);;
        Vector2 size = Vector2(0.f, 0.f);;
        Vector4 color = DirectX::Colors::White;
    };

    struct TextureSprite
    {
        TextureSprite(ID3D11Device* pDevice, LPCWSTR filename);

        ComPtr<ID3D11ShaderResourceView> pTextureView;
        ComPtr<ID3D11Resource> pResource;
        ComPtr<ID3D11Texture2D> pTexture;
        LPCWSTR filename;
        CD3D11_TEXTURE2D_DESC desc;

        float rotation = 0.0f; // in radians
        Vector2 position;
        DirectX::XMVECTOR color = DirectX::Colors::White;
        Vector2 origin;
    };
}
