#pragma once
#include "spriteHelper.h"

using namespace Microsoft::WRL;
using namespace DirectX::SimpleMath;

float SpriteHelper::angleDegreeFromRad(float radians)
{
    return radians * PI / 180;
};

Vector2 SpriteHelper::getFontSize(DirectX::SpriteFont* pSpriteFont, std::wstring outputText, float scale)
{
    DirectX::XMVECTOR temp = pSpriteFont->MeasureString(outputText.c_str());
    return Vector2(
        temp.m128_f32[0] * scale,
        temp.m128_f32[1] * scale
    );
};

Vector2 SpriteHelper::getFontOrigin(DirectX::SpriteFont* pSpriteFont, std::wstring outputText, float scale)
{
    return SpriteHelper::getFontSize(pSpriteFont, outputText, scale) / 2.0f;
};

SpriteHelper::FontSprite::FontSprite(float scale = 1.0f)
{
    this->scale = scale;
};

SpriteHelper::FontSprite::FontSprite(std::wstring outputText, DirectX::SpriteFont* pSpriteFont, float scale = 1.0f)
{
    this->scale = scale;
    this->setOutputText(outputText, pSpriteFont);
};

void SpriteHelper::FontSprite::setOutputText(std::wstring outputText, DirectX::SpriteFont* pSpriteFont)
{
    if (outputText == this->outputText) return;
    this->outputText = outputText;
    // this->outputText = const_cast<wchar_t*>(outputText);
    this->origin = SpriteHelper::getFontOrigin(pSpriteFont, this->outputText, this->scale);
    this->size = SpriteHelper::getFontSize(pSpriteFont, this->outputText, this->scale);

    // std::cout << "scale:" << this->scale << std::endl;
    // std::cout << "origin(x:" << this->origin.x << "y:" << this->origin.y << ")" << std::endl;
    // std::cout << "size(x:" << this->size.x << "y:" << this->size.y << ")" << std::endl;
};

Vector2 SpriteHelper::FontSprite::getFontOrigin(DirectX::SpriteFont* pSpriteFont)
{
    if (!this->origin.x && !this->origin.y)
    {
        std::cout << "FontSprite::getFontOrigin()" << std::endl;
        this->origin = SpriteHelper::getFontOrigin(pSpriteFont, this->outputText, this->scale);    
    }
    std::cout << "origin(x:" << this->origin.x << "y:" << this->origin.y << ")" << std::endl;
    return this->origin;
};

Vector2 SpriteHelper::FontSprite::getFontSize(DirectX::SpriteFont* pSpriteFont)
{
    if (!this->size.x && !this->size.y)
    {
        std::cout << "FontSprite::getFontSize()" << std::endl;
        this->size = SpriteHelper::getFontSize(pSpriteFont, this->outputText, this->scale);    
    }
    return this->size;
}

SpriteHelper::TextureSprite::TextureSprite(ID3D11Device* pDevice, LPCWSTR filename) {
    this->filename = filename;
    DirectX::CreateWICTextureFromFile(pDevice, this->filename, this->pResource.ReleaseAndGetAddressOf(), this->pTextureView.ReleaseAndGetAddressOf());
    this->pResource.As(&this->pTexture);
    this->pTexture->GetDesc(&this->desc);
    this->origin.x = float(this->desc.Width / 2);
    this->origin.y = float(this->desc.Height / 2);
};